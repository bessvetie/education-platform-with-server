import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { selectToken, selectStatus } from './store/slices/auth.js';

import Loading from './components/Loading.js';

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired
};

function PrivateRoute(props) {
  const {
    component: Component, ...rest
  } = props;

  const status = useSelector(selectStatus);
  const token = useSelector(selectToken);

  return (
    <Route
      {...rest}
      render={({ location }) => {
        if (status === 'pending') {
          return (
            <Loading withLogo />
          );
        }

        if (token) {
          return (
            <Component />
          );
        }

        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        );
      }}
    />
  );
}

export default PrivateRoute;
