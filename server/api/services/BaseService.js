class BaseService {
  static result(result) {
    return {
      success: result.success || true,
      status: result.status || 200,
      ...(result.cookies && { cookies: result.cookies }),
      ...(result.headers && { headers: result.headers }),
      ...(result.message && { message: result.message }),
      ...(result.data && { data: result.data })
    };
  }

  static redirect(options) {
    return {
      redirect: {
        status: options.status || 301,
        url: options.url
      }
    };
  }
}

module.exports = { BaseService };
