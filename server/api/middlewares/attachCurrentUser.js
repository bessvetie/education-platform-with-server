const { AvailabilityError } = require('../helpers/errorClasses.js');
const UserModel = require('../models/user.js');

async function attachCurrentUser(req, res, next) {
  const decodedUserId = req.token.sub;
  const user = await UserModel.findOne({ _id: decodedUserId });
  if (!user) {
    throw new AvailabilityError('User not found');
  }
  req.currentUser = user;
  return next();
}

module.exports = attachCurrentUser;
