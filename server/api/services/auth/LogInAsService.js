const { BaseService } = require('../BaseService.js');
const { AvailabilityError } = require('../../helpers/errorClasses.js');
const { createTokens } = require('./tokenUtils.js');

const UserModel = require('../../models/user.js');

class LogInAsService extends BaseService {
  static async run(ctx) {
    const { email } = ctx.body;

    const userRecord = await UserModel.findOne({ email });
    if (!userRecord) {
      throw new AvailabilityError('User not found');
    }

    const tokens = await createTokens(userRecord);

    return this.result({
      data: {
        user: userRecord.toObject(),
        tokens
      }
    });
  }
}

module.exports = { LogInAsService };
