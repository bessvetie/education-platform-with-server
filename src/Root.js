import { hot } from 'react-hot-loader/root';
import React, { Suspense } from 'react';
import {
  Switch, Route, Redirect, BrowserRouter
} from 'react-router-dom';

import {
  useMediaQuery, CssBaseline, createMuiTheme, ThemeProvider
} from '@material-ui/core';

import PrivateRoute from './PrivateRoute.js';
import Loading from './components/Loading.js';

import loginPage from './pages/login.js';
import registerPage from './pages/register.js';

import dashboardPage from './pages/dashboard.js';

// RHL doesn't work with React.lazy https://github.com/gaearon/react-hot-loader/issues/1425#issuecomment-675821519
// const loginPage = lazy(() => import(
//   /* webpackPreload: true */
//   /* webpackChunkName: "login" */
//   './pages/login'
// ));
// const registerPage = lazy(() => import(
//   /* webpackPreload: true */
//   /* webpackChunkName: "register" */
//   './pages/register'
// ));
// const dashboardPage = lazy(() => import(
//   /* webpackPreload: true */
//   /* webpackChunkName: "register" */
//   './pages/dashboard'
// ));

function Root() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const themeCommon = React.useMemo(
    () => createMuiTheme({
      palette: {
        type: 'dark', // prefersDarkMode ? 'dark' : 'light',
        primary: {
          main: '#192d3e',
          light: '#435669',
          dark: '#000218'
        },
        secondary: {
          main: '#007997',
          light: '#4fa8c8',
          dark: '#004d69'
        }
      }
    }),
    [prefersDarkMode]
  );
  const themeDifferent = (theme) => {
    const { type } = theme.palette;
    const isDark = type === 'dark';

    return createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        background: {
          paper: isDark ? '#1e2125' : '#fff',
          default: isDark ? '#121212' : '#fafafa'
        }
      }
    });
  };

  return (
    <ThemeProvider theme={themeCommon}>
      <CssBaseline />

      <ThemeProvider theme={themeDifferent}>
        <BrowserRouter>
          <Suspense fallback={<Loading withLogo />}>
            <Switch>
              <Route exact path="/login" component={loginPage} />
              <Route exact path="/register" component={registerPage} />
              <PrivateRoute exact path="/dashboard" component={dashboardPage} />

              <Redirect exact from="/" to="/dashboard" />
              <Redirect from="*" to="/" />
            </Switch>
          </Suspense>
        </BrowserRouter>
      </ThemeProvider>
    </ThemeProvider>
  );
}

export default hot(Root);
