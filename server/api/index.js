const path = require('path');
const { promises: fs } = require('fs');

const catchError = require('./middlewares/catchError.js');

async function* getFiles(folderName) {
  const folder = path.join(__dirname, folderName);
  const subdirs = await fs.readdir(folder);
  for (const item of subdirs) {
    const fullName = path.join(folderName, item);
    if ((await fs.stat(path.join(folder, item))).isDirectory()) {
      yield* getFiles(fullName);
    }
    else {
      yield fullName;
    }
  }
}

async function useRoutes(app, isProd) {
  for await (const file of getFiles('/routes')) {
    const importPath = `.${file.split(path.sep).join('/')}`;
    if (isProd) {
      const Controller = require(importPath);
      const controller = new Controller();
      await controller.init();

      const { endpoint, router } = controller;
      app.use(endpoint, router);
    }
    else {
      const endpoint = `/api/${path.basename(file, '.js')}`;
      app.use(endpoint, (req, res, next) => {
        const Controller = require(importPath);
        const controller = new Controller();

        controller.router(req, res, next);
      });
    }
  }

  catchError(app);
}

module.exports = useRoutes;
