/* eslint-disable max-classes-per-file */

class ApiError extends Error {
  constructor(error) {
    super(error);
    this.name = 'ApiError';
    this.message = error || 'Internal Server Error';
    Error.captureStackTrace(this, ApiError);
  }
}

class UnauthorizedError extends ApiError {
  constructor(error) {
    super(error);
    this.name = 'UnauthorizedError';
    this.message = error || 'Invalid credentials given';
  }
}

class AvailabilityError extends ApiError {
  constructor(error) {
    super(error);
    this.name = 'AvailabilityError';
    this.message = error || 'Content not found';
  }
}

class ForbiddenError extends ApiError {
  constructor(error) {
    super(error);
    this.name = 'ForbiddenError';
    this.message = error || 'Access denied';
  }
}

module.exports = {
  ApiError,
  UnauthorizedError,
  AvailabilityError,
  ForbiddenError
};
/* eslint-enable */
