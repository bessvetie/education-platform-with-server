import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, useLocation } from 'react-router-dom';

import { fetchLogIn, selectToken } from '../store/slices/auth.js';

import SignIn from '../layouts/SignIn.js';

function LoginPage() {
  const dispatch = useDispatch();
  const location = useLocation();

  const token = useSelector(selectToken);

  const { from } = location.state || { from: { pathname: '/' } };
  if (token) {
    return <Redirect to={from} />;
  }

  const submitForm = ({ email, password }) => {
    dispatch(fetchLogIn({ email, password }));
  };

  return (
    <SignIn isCreating={false} submitForm={submitForm} />
  );
}

export default LoginPage;
