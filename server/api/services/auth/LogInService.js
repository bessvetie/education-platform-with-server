const argon2 = require('argon2');

const { BaseService } = require('../BaseService.js');
const { UnauthorizedError } = require('../../helpers/errorClasses.js');
const { createTokens } = require('./tokenUtils.js');

const { ignoreModelFields } = require('../../helpers/utils.js');

const UserModel = require('../../models/user.js');

class LogInService extends BaseService {
  static async run(ctx) {
    const { email, password } = ctx.body;

    const userRecord = await UserModel.findOne({ email }).select('+password');
    if (!userRecord) {
      throw new UnauthorizedError();
    }
    else {
      const correctPassword = await argon2.verify(userRecord.password, password);
      if (!correctPassword) {
        throw new UnauthorizedError();
      }
    }

    const tokens = await createTokens(userRecord);

    return this.result({
      data: {
        user: ignoreModelFields(userRecord.toObject(), ['password']),
        tokens
      }
    });
  }
}

module.exports = { LogInService };
