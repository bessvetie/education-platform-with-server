const moment = require('moment-timezone');
const http = require('http');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');

const app = require('../server/app.js');

const isProd = process.env.NODE_ENV === 'production';

morgan.token('date', () => (
  moment().tz('Asia/Krasnoyarsk').format('YYYY-MM-DD, HH:mm a')
));
let requestsLogger = null;

const logsDir = '../server/logs';
if (!isProd) {
  requestsLogger = morgan('tiny', {
    stream: fs.createWriteStream(path.join(__dirname, `${logsDir}/dev.log`))
  });
}
else {
  requestsLogger = morgan('combined', {
    stream: fs.createWriteStream(path.join(__dirname, `${logsDir}/prod.log`), { flags: 'a' })
  });
}

/**
 * Get port from environment and store in Express. Create HTTP server.
 * Listen on provided port, on all network interfaces.
 */

(async function startServer() {
  const port = normalizePort(process.env.APP_PORT || '3000');

  let server = await app({ requestsLogger });
  server.set('port', port);
  server = http.createServer(server);

  server.listen(port);
  server.on('error', (error) => {
    onError(error);
  });
  server.on('listening', () => {
    onListening(server);
  });
}());

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const nPort = parseInt(val, 10);

  if (Number.isNaN(nPort)) {
    // named pipe
    return val;
  }

  if (nPort >= 0) {
    // port number
    return nPort;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      process.exit(1);
      break;
    case 'EADDRINUSE':
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening(server) {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;

  const ready = `> Ready on ${bind}`;
  console.log(ready);
}
