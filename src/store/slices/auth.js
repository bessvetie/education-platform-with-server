import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

import client from '../api/client.js';

import { userLoggedIn } from './users.js';

export const fetchLogIn = createAsyncThunk(
  'auth/fetchLogIn',
  async (loginData, { dispatch }) => {
    const response = await client.post('auth/login', loginData);
    const { tokens, user } = response.data;
    dispatch(userLoggedIn(user));
    return tokens.accessToken;
  }
);

export const fetchSignUp = createAsyncThunk(
  'auth/fetchSignUp',
  async (registerData, { dispatch }) => {
    const response = await client.post('auth/signup', registerData);
    const { tokens, user } = response.data;
    dispatch(userLoggedIn(user));
    return tokens.accessToken;
  }
);

const authPending = (state) => {
  state.status = 'pending';
  state.error = null;
};

const authFulfilled = (state, { payload }) => {
  state.status = 'fulfilled';
  state.accessToken = payload;
};

const authRejected = (state, { error }) => {
  state.status = 'rejected';
  state.error = error.message;
};

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    accessToken: null,
    status: 'idle',
    error: null
  },
  reducers: {},
  extraReducers: {
    [fetchLogIn.pending]: authPending,
    [fetchLogIn.fulfilled]: authFulfilled,
    [fetchLogIn.rejected]: authRejected,

    [fetchSignUp.pending]: authPending,
    [fetchSignUp.fulfilled]: authFulfilled,
    [fetchSignUp.rejected]: authRejected
  }
});

export const selectToken = (state) => state.auth.accessToken;
export const selectStatus = (state) => state.auth.status;

export default authSlice.reducer;
