class BaseController {
  constructor() {
    if (!this.init) throw new Error(`${this.constructor.name} should implement 'init' method.`);
    if (!this.router) throw new Error(`${this.constructor.name} should implement 'router' getter.`);
  }

  serviceRunner(service) {
    if (!Object.prototype.hasOwnProperty.call(service, 'run')) {
      throw new Error(`${this.constructor.name}: 'run' method not declared in invoked '${service.name}' service`);
    }

    return async (req, res, next) => {
      const ctx = {
        currentUser: req.currentUser,
        body: req.body,
        query: req.query,
        params: req.params,
        ip: req.ip,
        method: req.method,
        url: req.url,
        cookies: req.cookies,
        headers: {
          'Content-Type': req.get('Content-Type'),
          Referer: req.get('referer'),
          'User-Agent': req.get('User-Agent')
        }
      };

      try {
        /**
         * run service
         */
        const response = await service.run(ctx);

        /**
         * set headers
         */
        if (response.headers) res.set(response.headers);

        /**
         * set cookie
         */
        if (response.cookies && response.cookies.length) {
          for (const cookie of response.cookies) {
            res.cookie(cookie.name, cookie.value, cookie.options);
          }
        }

        /**
         * optional redirect
         */
        if (response.redirect) return res.redirect(response.redirect.status, response.redirect.url);

        /**
         * set status and return result to client
         */
        return res.status(response.status).json({
          success: response.success,
          message: response.message,
          data: response.data
        });
      }
      catch (error) {
        error.req = ctx;
        return next(error);
      }
    };
  }
}

module.exports = { BaseController };
