// A tiny wrapper around fetch(), borrowed from
// https://kentcdodds.com/blog/replace-axios-with-a-simple-custom-fetch-wrapper

export default async function client(endpoint, { body, ...customConfig } = {}) {
  const headers = { 'Content-Type': 'application/json' };

  const { apiURL = process.env.APP_URL, ...restConfig } = customConfig;

  const config = {
    method: body ? 'POST' : 'GET',
    ...restConfig,
    headers: {
      ...headers,
      ...restConfig.headers
    }
  };

  if (body) {
    config.body = JSON.stringify(body);
  }

  let data;
  try {
    const response = await window.fetch(`${apiURL}/${endpoint}`, config);
    data = await response.json();
    if (response.ok) {
      return data;
    }
    throw new Error(response.statusText);
  }
  catch (err) {
    return Promise.reject(err.message ? err.message : data);
  }
}

client.get = (endpoint, customConfig = {}) => (
  client(endpoint, {
    ...customConfig,
    method: 'GET'
  })
);

client.post = (endpoint, body, customConfig = {}) => (
  client(endpoint, {
    ...customConfig,
    method: 'POST',
    body
  })
);
