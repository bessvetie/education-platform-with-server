function filterModelFields(modelRecord, allowedFields) {
  return Object.keys(modelRecord)
    .reduce((obj, key) => {
      if (allowedFields.includes(key)) {
        return {
          ...obj,
          [key]: modelRecord[key]
        };
      }

      return obj;
    }, {});
}

function ignoreModelFields(modelRecord, ignoredFields) {
  return Object.keys(modelRecord)
    .reduce((obj, key) => {
      if (!ignoredFields.includes(key)) {
        return {
          ...obj,
          [key]: modelRecord[key]
        };
      }

      return obj;
    }, {});
}

module.exports = {
  filterModelFields,
  ignoreModelFields
};
