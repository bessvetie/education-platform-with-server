import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button, TextField, FormControlLabel, Checkbox, Grid, Typography, Box,
  makeStyles
} from '@material-ui/core';

import CustomLink from './CustomLink.js';

SignForm.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  submitForm: PropTypes.func.isRequired
};

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function SignForm(props) {
  const { isCreating, submitForm } = props;
  const classes = useStyles();

  const [name, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [isRemember, setRemember] = useState(false);

  const onUsernameChanged = (e) => setUsername(e.target.value);
  const onEmailChanged = (e) => setEmail(e.target.value);
  const onPasswordChanged = (e) => setPassword(e.target.value);
  const onPasswordConfirmChanged = (e) => setPasswordConfirm(e.target.value);
  const onRememberChanged = (e) => setRemember(e.target.checked);

  const onSubmitForm = (e) => {
    e.preventDefault();
    submitForm({
      name,
      email,
      password
    });
  };

  return (
    <form className={classes.form} noValidate onSubmit={onSubmitForm}>
      {isCreating && (
        <TextField
          value={name}
          onChange={onUsernameChanged}
          size="small"
          variant="outlined"
          color="secondary"
          margin="normal"
          required
          fullWidth
          id="name"
          label="Username"
          name="name"
          autoComplete="name"
          autoFocus
        />
      )}
      <TextField
        value={email}
        onChange={onEmailChanged}
        size="small"
        variant="outlined"
        color="secondary"
        margin="normal"
        required
        fullWidth
        id="email"
        label="Email Address"
        name="email"
        autoComplete="email"
      />
      <TextField
        value={password}
        onChange={onPasswordChanged}
        size="small"
        variant="outlined"
        color="secondary"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
      />
      {isCreating && (
        <TextField
          value={passwordConfirm}
          onChange={onPasswordConfirmChanged}
          size="small"
          variant="outlined"
          color="secondary"
          margin="normal"
          required
          fullWidth
          name="password-confirm"
          label="Confirm Password"
          type="password"
          id="password-confirm"
        />
      )}
      <FormControlLabel
        control={(
          <Checkbox
            value="remember"
            color="secondary"
            variant="buffer"
            checked={isRemember}
            onChange={onRememberChanged}
          />
        )}
        label="Remember me"
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="secondary"
        className={classes.submit}
      >
        {!isCreating ? 'Login' : 'Register'}
      </Button>
      {!isCreating ? (
        <Grid direction="column" container>
          <Box mb={2} component={Grid} justify="flex-end" container>
            <CustomLink color="secondary" to="/forgot_password">
              Forgot password?
            </CustomLink>
          </Box>
          <Grid direction="column" justify="center" alignItems="center" container>
            <Typography variant="caption">
              {'Don\'t have an account?'}
            </Typography>
            <CustomLink color="secondary" to="/register">
              Sign Up
            </CustomLink>
          </Grid>
        </Grid>
      ) : (
        <Grid direction="column" justify="center" alignItems="center" container>
          <Typography variant="caption">
            Already have an account?
          </Typography>
          <CustomLink color="secondary" to="/login">
            Sign In
          </CustomLink>
        </Grid>
      )}
    </form>
  );
}

export default SignForm;
