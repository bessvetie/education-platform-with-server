const express = require('express');

const { BaseController } = require('../BaseController.js');

const { SignUpService } = require('../services/auth/SignUpService.js');
const { LogInService } = require('../services/auth/LogInService.js');
const { LogInAsService } = require('../services/auth/LogInAsService.js');

const checkRole = require('../middlewares/checkRole.js');
const isAuth = require('../middlewares/isAuth.js');
const attachCurrentUser = require('../middlewares/attachCurrentUser.js');

const router = express.Router();

class AuthController extends BaseController {
  static get endpoint() {
    return '/api/auth';
  }

  get router() {
    router.post(
      '/signup',
      this.serviceRunner(SignUpService)
    );

    router.post(
      '/login',
      this.serviceRunner(LogInService)
    );

    // The middlewares need to be placed this way because they depend upon each other
    router.post(
      '/login-as',
      isAuth,
      attachCurrentUser,
      checkRole('admin'),
      this.serviceRunner(LogInAsService)
    );

    return router;
  }

  async init() {
    console.log(`${this.constructor.name} initialized...`);
  }
}

module.exports = AuthController;
