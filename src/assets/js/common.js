// Like https://github.com/brunobertolini/styled-by
export const styledBy = (property, mapping) => (props) => mapping[props[property]];

export default {
  styledBy
};
