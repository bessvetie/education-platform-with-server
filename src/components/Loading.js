import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Box, CircularProgress } from '@material-ui/core';
import { School } from '@material-ui/icons';

Loading.propTypes = {
  withLogo: PropTypes.bool
};

Loading.defaultProps = {
  withLogo: false
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100vh',
    backgroundColor: 'transparent'
  },
  circular: {
    color: theme.palette.secondary.light
  }
}));

function Loading(props) {
  const { withLogo } = props;
  const classes = useStyles(props);

  return (
    <Box className={classes.root} zIndex="modal">
      {withLogo && (
        <School style={{ fontSize: 150 }} />
      )}
      <CircularProgress className={classes.circular} />
    </Box>
  );
}

export default Loading;
