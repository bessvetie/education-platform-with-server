function catchError(app) {
  app.use((err, req, res, next) => {
    let statusCode = 500;
    let { message: errMessage } = err;
    const { name: errName, code: errCode } = err;

    if (errName === 'UnauthorizedError') {
      statusCode = 401;
    }
    else if (errName === 'ForbiddenError') {
      statusCode = 403;
    }
    else if (errName === 'AvailabilityError') {
      statusCode = 404;
    }
    else if (errName === 'MongoError' && errCode === 11000) {
      statusCode = 409;
      errMessage = `Such ${Object.keys(err.keyValue).join(' ')} already exists`;
    }

    return res.status(statusCode)
      .json({ error: errMessage })
      .end();
  });
}

module.exports = catchError;
