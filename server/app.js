const express = require('express');
const path = require('path');
const chokidar = require('chokidar');
const mongoose = require('mongoose');

const pkg = require('../package.json');

const useRoutes = require('./api/index.js');

function setDevEnv(app, isProd) {
  if (!isProd) {
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const config = require('../webpack.config.js');

    const [modernConfig, legacyConfig] = config;

    // doesn't reload if the name is wrong
    modernConfig.entry.modern.unshift('webpack-hot-middleware/client?name=modernConfig&&timeout=20000&&reload=true');
    modernConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

    if (legacyConfig) {
      legacyConfig.entry.legacy.unshift('webpack-hot-middleware/client?name=legacyConfig&&timeout=20000&&reload=true');
      legacyConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
    }

    const compiler = webpack(config);

    app.use(webpackDevMiddleware(compiler, {
      publicPath: '/',
      writeToDisk: true,
      stats: 'minimal'
    }));

    app.use(webpackHotMiddleware(compiler, {
      path: '/__webpack_hmr'
    }));

    const watcher = chokidar.watch(path.join(__dirname, '/api'), {
      ignored: path.join(__dirname, '/api/index.js')
    });

    watcher.on('ready', () => {
      watcher.on('all', (event) => {
        console.log('caches have been deleted:');
        Object.keys(require.cache).forEach((id) => {
          if (id.includes(`server${path.sep}api`) && !id.includes('models')) {
            console.log(event, id);
            delete require.cache[id];
          }
        });
      });
    });
  }
}

async function initApp({ requestsLogger }) {
  const publicPath = path.join(__dirname, `../${pkg.config.publicDir}`);
  const isProd = process.env.NODE_ENV === 'production';

  const app = express();

  app.use(requestsLogger);
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  await mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  setDevEnv(app, isProd);

  app.use(express.static(publicPath));

  await useRoutes(app, isProd);

  app.get('/*', (req, res) => {
    res.sendFile(path.join(publicPath, '/index.html'));
  });

  return app;
}

module.exports = initApp;
