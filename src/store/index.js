import { configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';

import rootReducer from './rootReducer.js';

const middleware = [];
if (process.env.NODE_ENV === 'development') {
  middleware.push(logger);
}

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(middleware),
  devTools: process.env.NODE_ENV !== 'production'
});

export default store;
