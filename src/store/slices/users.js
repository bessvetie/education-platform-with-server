import { createSlice } from '@reduxjs/toolkit';

export const usersSlice = createSlice({
  name: 'users',
  initialState: {
    activeUser: null
  },
  reducers: {
    userLoggedIn: (state, { payload }) => {
      state.activeUser = payload;
    }
  }
});

export const { userLoggedIn } = usersSlice.actions;

export const selectActiveUser = (state) => state.users.activeUser;

export default usersSlice.reducer;
