import authReducer from './slices/auth.js';
import usersReducer from './slices/users.js';

export default {
  auth: authReducer,
  users: usersReducer
};
