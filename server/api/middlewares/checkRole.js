const { ForbiddenError } = require('../helpers/errorClasses.js');

function checkRole(requiredRole) {
  return (req, res, next) => {
    if (req.currentUser.role !== requiredRole) {
      throw new ForbiddenError();
    }

    return next();
  };
}

module.exports = checkRole;
