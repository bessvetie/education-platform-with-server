const path = require('path');
const { promises: fs } = require('fs');
const jwt = require('jsonwebtoken');
const { randomBytes } = require('crypto');

const { ApiError, UnauthorizedError } = require('../../helpers/errorClasses.js');

const KeystoreModel = require('../../models/keystore.js');

function readPublicKey() {
  return fs.readFile(path.join(__dirname, '../../../keys/public.pem'), 'utf8');
}

function readPrivateKey() {
  return fs.readFile(path.join(__dirname, '../../../keys/private.pem'), 'utf8');
}

async function encode(jwtPayload) {
  const cert = await readPrivateKey();
  if (!cert) throw new ApiError('Token generation failure');

  return jwt.sign({ ...jwtPayload }, cert, { algorithm: 'RS256' });
}

/**
 * This method checks the token and returns the decoded data when token is valid in all respect
 */
async function validate(token) {
  const cert = await readPublicKey();
  try {
    return jwt.verify(token, cert);
  }
  catch (e) {
    if (e && e.name === 'TokenExpiredError') {
      throw new UnauthorizedError('Token has expired');
    }

    // throws error if the token has not been encrypted by the private key
    throw new UnauthorizedError('Token has not been encrypted');
  }
}

/**
 * Returns the decoded payload if the signature is valid even if it is expired
 */
async function decode(token) {
  const cert = await readPublicKey();
  try {
    return jwt.verify(token, cert, { ignoreExpiration: true });
  }
  catch (e) {
    throw new UnauthorizedError();
  }
}

async function createTokens(user) {
  const timestamp = Date.now();

  const accessToken = await encode(
    createJwtPayload(
      user._id,
      randomBytes(64).toString('hex'),
      process.env.TOKEN_ACCESS_EXP_MIN,
      timestamp
    )
  );

  if (!accessToken) {
    throw new ApiError('Access Token generation failure');
  }

  const refreshToken = await encode(
    createJwtPayload(
      user._id,
      randomBytes(64).toString('hex'),
      process.env.TOKEN_REFRESH_EXP_MIN,
      timestamp
    )
  );

  if (!refreshToken) {
    throw new ApiError('Refresh Token generation failure');
  }

  const keystore = await KeystoreModel.create({
    client: user._id,
    accessToken,
    refreshToken,
    createdAt: timestamp,
    updatedAt: timestamp
  });

  return keystore.toObject();
}

function createJwtPayload(subject, salt, validityInMinutes, timestamp) {
  const issuedAt = Math.floor(timestamp / 1000);
  const expirationTime = issuedAt + validityInMinutes * 60;

  return {
    sub: subject,
    iat: issuedAt,
    exp: expirationTime,
    slt: salt
  };
}

module.exports = {
  createTokens,
  validate,
  decode
};
