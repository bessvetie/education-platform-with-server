const argon2 = require('argon2');
const { randomBytes } = require('crypto');

const { BaseService } = require('../BaseService.js');
const { createTokens } = require('./tokenUtils.js');

const UserModel = require('../../models/user.js');

class SignUpService extends BaseService {
  static async run(ctx) {
    const {
      name, email, password, role
    } = ctx.body;

    const salt = randomBytes(32);

    const passwordHashed = await argon2.hash(password, { salt });

    const now = new Date();

    const user = {
      createdAt: now,
      updatedAt: now,
      email,
      name,
      role
    };

    const userRecord = await UserModel.create({
      ...user,
      password: passwordHashed,
      salt: salt.toString('hex')
    });

    const tokens = await createTokens(userRecord);

    return this.result({
      data: {
        user: {
          ...user,
          role: userRecord.role
        },
        tokens
      }
    });
  }
}

module.exports = { SignUpService };
