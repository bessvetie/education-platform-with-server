const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserSchema = new Schema({
  email: {
    type: Schema.Types.String,
    required: true,
    unique: true,
    lowercase: true,
    select: true
  },

  password: {
    type: Schema.Types.String,
    required: true,
    select: false
  },

  salt: {
    type: Schema.Types.String,
    required: true,
    select: false
  },

  name: {
    type: Schema.Types.String,
    required: true,
    unique: true,
    select: true
  },

  role: {
    type: Schema.Types.String,
    default: 'user', // Possible values: user | admin
    select: true
  },

  createdAt: {
    type: Schema.Types.Date,
    required: true,
    select: true
  },

  updatedAt: {
    type: Schema.Types.Date,
    required: true,
    select: true
  }
});

module.exports = mongoose.model('User', UserSchema);
