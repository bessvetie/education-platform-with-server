const { validate } = require('../services/auth/tokenUtils.js');

async function getTokenFromHeader(req, res, next) {
  const { authorization = '' } = req.headers;
  const [type, token] = authorization.split(' ');

  try {
    req.token = await validate(type === 'Bearer' ? token : null);
    return next();
  }
  catch (e) {
    return next(e);
  }
}

module.exports = getTokenFromHeader;
