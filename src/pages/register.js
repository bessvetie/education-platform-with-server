import React from 'react';
import { useDispatch } from 'react-redux';

import { fetchSignUp } from '../store/slices/auth.js';

import SignIn from '../layouts/SignIn.js';

function RegisterPage() {
  const dispatch = useDispatch();

  const submitForm = ({ name, email, password }) => {
    dispatch(fetchSignUp({ name, email, password }));
  };

  return (
    <SignIn isCreating submitForm={submitForm} />
  );
}

export default RegisterPage;
