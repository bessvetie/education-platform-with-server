import React from 'react';
import PropTypes from 'prop-types';
import {
  Avatar, Paper, Grid, Typography, Box,
  makeStyles
} from '@material-ui/core';
import { AccountCircle, School } from '@material-ui/icons';

import SignInForm from '../components/SignInForm.js';

SignIn.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  submitForm: PropTypes.func.isRequired
};

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh'
  },
  left: {
    background: theme.palette.type === 'dark' ?
      `linear-gradient(to right, ${theme.palette.background.default}, ${theme.palette.primary.main})` :
      `linear-gradient(to right, ${theme.palette.secondary.main}, ${theme.palette.background.default})`
  },
  right: {
    width: '20rem'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  }
}));

function SignIn(props) {
  const { isCreating, submitForm } = props;
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.root}>
      <Grid item xs className={classes.left}>
        <Box p={15} maxWidth="58rem">
          <School style={{ fontSize: 150 }} />
          <Box pt={2}>
            <Typography variant="h3" gutterBottom>
              Welcome to the traiSTARTing!
            </Typography>
            <Typography variant="subtitle1">
              With traiSTARTing you can study whenever and wherever you choose.
              Our flexible teaching also means, if you travel often or need to relocate,
              you can continue to study wherever you go.
            </Typography>
          </Box>
        </Box>
      </Grid>
      <Grid className={classes.right} component={Paper} elevation={6} square>
        <Box display="flex" flexDirection="column" alignItems="center" my={8} mx={3}>
          <Avatar className={classes.avatar}>
            <AccountCircle />
          </Avatar>
          <Typography component="h1" variant="h5">
            {isCreating ? 'Create An Account' : 'Login To Your Account'}
          </Typography>
          <SignInForm isCreating={isCreating} submitForm={submitForm} />
        </Box>
      </Grid>
    </Grid>
  );
}

export default SignIn;
