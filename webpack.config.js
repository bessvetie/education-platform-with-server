const path = require('path');

const webpack = require('webpack');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const pkg = require('./package.json');

const isProd = process.env.NODE_ENV === 'production';
const mode = isProd ? 'production' : 'development';

const publicDir = path.resolve(__dirname, `${pkg.config.publicDir}`);
const publicPath = '/';

const hash = isProd ? '.[chunkhash]' : '';

const withLegacy = process.env.LEGACY_CODE === 'include';

const data = Object.create(null);

const configurePlugins = () => ([
  // Identify each module by a hash, so caching is more predictable.
  new webpack.HashedModuleIdsPlugin(),

  // Extract CSS into separate files per JS file which contains CSS
  new MiniCssExtractPlugin({
    filename: `styles/[name]${hash}.css`,
    chunkFilename: `styles/[name]${hash}.css`
  }),

  new WebpackAssetsManifest({
    output: 'assets-manifest.json',
    merge: true,
    assets: data,
    publicPath,
    entrypoints: true,
    entrypointsKey: false
  }),

  new HtmlWebpackPlugin({
    title: 'traiSTARTing',
    template: './src/views/template.ejs',
    assetsManifest: data,
    withLegacy,
    meta: {
      viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
    },
    inject: false
  }),

  new webpack.DefinePlugin({
    'process.env.APP_URL': JSON.stringify(process.env.APP_URL)
  })
]);

const configureBabelLoader = (browserlist) => ({
  test: /\.m?js$/,
  use: {
    loader: 'babel-loader',
    options: {
      babelrc: false,
      exclude: /(node_modules|bower_components|server)/,
      presets: [
        ['@babel/preset-env', {
          loose: true,
          modules: false,
          // debug: true,
          corejs: 3,
          useBuiltIns: 'usage',
          targets: {
            browsers: browserlist
          }
        }]
      ],
      plugins: [
        'react-hot-loader/babel',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-transform-react-jsx'
      ]
    }
  }
});

const configureEslintLoader = () => ({
  test: /\.js$/,
  enforce: 'pre',
  exclude: /node_modules/,
  loader: 'eslint-loader'
});

const configureStylesLoader = () => ({
  test: /\.(sa|sc|c)ss$/,
  use: [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        hmr: !isProd
      }
    },
    'css-loader',
    'postcss-loader',
    'sass-loader'
  ]
});

const configureImagesLoader = () => ({
  test: /\.(jpe?g|png|gif|svg)$/i,
  include: path.join(__dirname, 'src/assets/images'),
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name].[contenthash].[ext]',
        outputPath: 'images'
      }
    }
  ]
});

const configureFontsLoader = () => ({
  test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'fonts'
      }
    }
  ]
});

const commonLoaders = [
  configureEslintLoader(),
  configureStylesLoader(),
  configureImagesLoader(),
  configureFontsLoader()
];

const baseConfig = () => ({
  output: {
    path: publicDir,
    publicPath,
    filename: `js/[name]${hash}.js`,
    chunkFilename: `js/[name]${hash}.js`
  },
  stats: !isProd ? 'errors-only' : 'normal',
  mode,
  cache: {},
  resolve: {
    extensions: ['.mjs', '.js', '.json', '.jsx'],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    }
  },
  devtool: 'source-map',
  // devtool: !isProd ? 'eval-source-map' : 'source-map',
  optimization: {
    minimizer: [
      new TerserJSPlugin({}),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: configurePlugins()
});

const modernConfig = {
  name: 'modernConfig',
  entry: {
    modern: ['./src/main-module.js']
  },

  ...baseConfig(),
  optimization: {
    runtimeChunk: true,
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        bundle: {
          test: /[\\/]node_modules[\\/](((?!react).)*?)([\\/]|$)/,
          name(module) {
            if (module.context.includes('node_modules')) {
              // The directory name following the last `node_modules`.
              // Usually this is the package, but it could also be the scope.

              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              let packageName = module.context.match(/[\\/]node_modules[\\/](((?!react).)*?)([\\/]|$)/);
              if (packageName) {
                packageName = packageName[1];
              }

              let packageReact = module.context.match(/[\\/]node_modules[\\/](.*?react.*?)([\\/]|$)/);
              if (packageReact) {
                packageReact = packageReact[1];
              }

              // Group react dependencies into a common "react" chunk.
              // NOTE: This isn't strictly necessary for this app, but it's included
              // as an example to show how to manually group common dependencies.
              if (packageReact || ['prop-types', 'scheduler', 'classnames', 'gud'].includes(packageName)) {
                return 'npm.react';
              }

              if (packageName.includes('lodash')) {
                return 'npm.lodash';
              }

              // Group `tslib` and `dynamic-import-polyfill` into the default bundle.
              // NOTE: This isn't strictly necessary for this app, but it's included
              // to show how to manually keep deps in the default chunk.
              if (packageName === 'tslib' || packageName === 'dynamic-import-polyfill') {
                return null;
              }

              // Otherwise just return the name.
              // npm package names are URL-safe, but some servers don't like @ symbols
              return `npm.${packageName.replace('@', '')}`;
            }

            return null;
          }
        },
        styles: {
          test: /\.(sa|sc|c)ss$/,
          name: 'styles',
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      ...commonLoaders,
      configureBabelLoader([
        // The last two versions of each browser, excluding versions
        // that don't support <script type="module">.
        'last 2 Chrome versions', 'not Chrome < 60',
        'last 2 Safari versions', 'not Safari < 10.1',
        'last 2 iOS versions', 'not iOS < 10.3',
        'last 2 Firefox versions', 'not Firefox < 54',
        'last 2 Edge versions', 'not Edge < 15'
      ])
    ]
  }
};

const legacyConfig = {
  name: 'legacyConfig',
  entry: {
    legacy: ['./src/main-nomodule.js']
  },

  ...baseConfig(),
  optimization: {
    runtimeChunk: true,
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        styles: {
          test: /\.(sa|sc|c)ss$/,
          name: 'styles',
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  module: {
    rules: [
      ...commonLoaders,
      configureBabelLoader([
        '> 1%',
        'last 2 versions',
        'Firefox ESR'
      ])
    ]
  }
};

const builds = [modernConfig];
if (withLegacy) {
  builds.push(legacyConfig);
}

module.exports = builds;
