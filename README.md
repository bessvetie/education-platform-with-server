# Platform For Education

## Node version
```shell
v14.0.0
```

## Custom commands
`yarn install` - Install dependencies.

`yarn dev` - Run development build with hot reload. If it doesn't start, make sure you aren't running anything else in the same port.

**Default PORT=3300**

`yarn build` - Run production build.
