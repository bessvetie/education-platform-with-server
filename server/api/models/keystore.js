const mongoose = require('mongoose');

const { Schema } = mongoose;

const KeystoreSchema = new Schema({
  client: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
    select: true
  },
  accessToken: {
    type: Schema.Types.String,
    required: true,
    select: true
  },
  refreshToken: {
    type: Schema.Types.String,
    required: true,
    select: true
  },
  createdAt: {
    type: Schema.Types.Date,
    required: true,
    select: false
  },
  updatedAt: {
    type: Schema.Types.Date,
    required: true,
    select: false
  }
});

KeystoreSchema.index({
  client: 1,
  accessToken: 1,
  refreshToken: 1
});

module.exports = mongoose.model('Keystore', KeystoreSchema);
