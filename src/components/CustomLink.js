import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import {
  Link,
  makeStyles, lighten
} from '@material-ui/core';

import { styledBy } from '../assets/js/common.js';

CustomLink.propTypes = {
  color: PropTypes.string
};

CustomLink.defaultProps = {
  color: 'primary'
};

const useStyles = makeStyles((theme) => {
  const isDark = theme.palette.type === 'dark';

  return {
    root: {
      color: styledBy('color', {
        primary: isDark ? lighten(theme.palette.primary.light, 0.3) : theme.palette.primary.light,
        secondary: isDark ? lighten(theme.palette.secondary.light, 0.3) : theme.palette.secondary.light
      })
    }
  };
});

function CustomLink(props) {
  const { color, ...rest } = props;
  const classes = useStyles(props);

  return (
    <Link className={classes.root} component={RouterLink} color="inherit" {...rest} />
  );
}

export default CustomLink;
